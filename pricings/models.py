from django.db import models
from datetime import datetime, timezone


class PricingModel(models.Model):
    
    a_coefficient = models.IntegerField()
    b_coefficient = models.IntegerField()
