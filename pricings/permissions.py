from rest_framework.permissions import BasePermission


class PricingPermission(BasePermission):
    
    def has_permission(self, request, view):
        
        return request.user.is_superuser and request.user.is_staff