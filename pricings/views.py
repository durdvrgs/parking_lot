from rest_framework.views           import APIView
from rest_framework.response        import Response
from rest_framework                 import status
from rest_framework.authentication  import TokenAuthentication
from rest_framework.permissions     import IsAuthenticated

from .models import PricingModel
from .serializers import PricingSerializer
from .permissions import PricingPermission


class PricingView(APIView):
    
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated, PricingPermission]
    
    def post(self, request):
        
        serializer = PricingSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)
        
        new_pricing =   PricingModel.objects.create(**request.data)
        serializer =    PricingSerializer(new_pricing).data
        
        return Response(serializer, status=status.HTTP_201_CREATED)

