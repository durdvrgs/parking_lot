from django.shortcuts import get_list_or_404
from levels.models import LevelModel
from datetime import datetime, timezone


class VehicleServices():
    
    @staticmethod
    def get_available_space_or_none(level, vehicle_type):
        
        available_spaces = level.spacemodel_set.filter(
            variety=vehicle_type, vehiclemodel=None
        ).order_by('id')
        
        if available_spaces:
            
            return available_spaces[0]
        
        return None
    
    @staticmethod
    def find_space_or_404(vehicle_type):
        
        levels_by_priority = LevelModel.objects.order_by('fill_priority')
        
        for level in levels_by_priority:
            
            space = VehicleServices.get_available_space_or_none(level, vehicle_type)
            
            if space:
                
                return space
        
        return 404
    
    @staticmethod
    def verify_vehicle_type(vehicle_type):
        
        if vehicle_type not in ['motorcycle', 'car']:
            
            return True
        
        return False
    
    @staticmethod
    def set_free_space_and_amount_paid(pricing, vehicle):
        
        vehicle.paid_at =   datetime.now(timezone.utc)
        time_parking =      round((vehicle.paid_at - vehicle.arrived_at).total_seconds() / 3600)
        
        vehicle.amount_paid = pricing.a_coefficient + pricing.b_coefficient * time_parking
        vehicle.space = None
        
        vehicle.save()