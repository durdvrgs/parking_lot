from django.shortcuts import get_list_or_404, get_object_or_404

from rest_framework.views           import APIView
from rest_framework.response        import Response
from rest_framework                 import status

from .models import VehicleModel
from .serializers import VehicleSerializer
from .services import VehicleServices

from levels.models import LevelModel, SpaceModel
from pricings.models import PricingModel


class VehicleView(APIView):
    
    def post(self, request):
        
        verify_pricings_exist = get_list_or_404(PricingModel)
        is_invalid_vehicle_type = VehicleServices.verify_vehicle_type(request.data['vehicle_type'])
        serializer = VehicleSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        if is_invalid_vehicle_type:
            return Response({'message': 'invalid vehicle type'}, status=status.HTTP_400_BAD_REQUEST)
        
        
        available_space = VehicleServices.find_space_or_404(request.data['vehicle_type'])
        
        if available_space == 404:
            return Response({'message': 'not spaces available'}, status=status.HTTP_404_NOT_FOUND)
        
        new_vechicle = VehicleModel.objects.create(**request.data, space=available_space)
        serializer = VehicleSerializer(new_vechicle).data
               
        return Response(serializer, status=status.HTTP_201_CREATED)
    
    def put(self, request, vehicle_id):
        
        last_pricing = get_list_or_404(PricingModel)[-1]
        this_vehicle = get_object_or_404(VehicleModel, id=vehicle_id)
        
        if this_vehicle.space == None:
            return Response({'error': 'vehicle not found'}, status=status.HTTP_404_NOT_FOUND)
        
        VehicleServices.set_free_space_and_amount_paid(last_pricing, this_vehicle)
        serializer = VehicleSerializer(this_vehicle).data
        
        return Response(serializer, status=status.HTTP_200_OK)