from django.db import models
from levels.models import LevelModel, SpaceModel, SpaceType


class VehicleModel(models.Model):
    
    vehicle_type =      models.CharField(max_length=255, choices=SpaceType.choices)
    license_plate =     models.CharField(max_length=255)
    arrived_at =        models.DateTimeField(auto_now_add=True)
    paid_at =           models.DateTimeField(blank=True, null=True)
    amount_paid =       models.IntegerField(blank=True, null=True)
    
    space =             models.OneToOneField(SpaceModel, on_delete=models.CASCADE, null=True)
