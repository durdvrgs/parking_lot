from rest_framework import serializers
from .models import SpaceType


class LevelSerializer(serializers.Serializer):
    
    id =                    serializers.IntegerField(read_only=True)
    
    name =                  serializers.CharField()
    fill_priority =         serializers.IntegerField()
    motorcycle_spaces =     serializers.IntegerField(write_only=True)
    car_spaces =            serializers.IntegerField(write_only=True)
    
    available_spaces =      serializers.SerializerMethodField(
        'get_available_spaces', read_only=True
    )
    
    def get_available_spaces(self, level):
        
        available_motorcycle_spaces = len(level.spacemodel_set.filter(
            variety=SpaceType.MOTORCYCLE, vehiclemodel=None
        ))
        
        available_car_spaces = len(level.spacemodel_set.filter(
            variety=SpaceType.CAR, vehiclemodel=None
        ))
        
        return {
            'available_motorcycle_spaces': available_motorcycle_spaces,
            'available_car_spaces': available_car_spaces
        }

class SpaceSerializer(serializers.Serializer):
    
    id = serializers.IntegerField(read_only=True)
    variety = serializers.CharField()
    
    level_name = serializers.SerializerMethodField(
        'get_floor'
    )

    def get_floor(self, spacemodel):
        
        return spacemodel.level.name