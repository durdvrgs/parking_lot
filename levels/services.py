from .serializers import LevelSerializer
from .models import SpaceModel, SpaceType


class LevelServices():
    
    @staticmethod
    def set_available_spaces(spaces, level):
        
        for _ in range(spaces['motorcycle_spaces']):
            
            SpaceModel.objects.create(variety=SpaceType.MOTORCYCLE, level=level)
            
        for _ in range(spaces['car_spaces']):
            
            SpaceModel.objects.create(variety=SpaceType.CAR, level=level)