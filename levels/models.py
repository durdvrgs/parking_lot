from django.db import models


class LevelModel(models.Model):
    
    name =              models.CharField(max_length=255) 
    fill_priority =     models.IntegerField()
    
class SpaceType(models.TextChoices):
    
    MOTORCYCLE = 'motorcycle', 'motorcycle'
    CAR = 'car', 'car',
    
class SpaceModel(models.Model):
    
    variety = models.CharField(max_length=255, choices=SpaceType.choices)
    level = models.ForeignKey(LevelModel, on_delete=models.CASCADE)