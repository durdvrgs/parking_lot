from django.shortcuts               import get_list_or_404

from rest_framework.views           import APIView
from rest_framework.response        import Response
from rest_framework                 import status
from rest_framework.authentication  import TokenAuthentication
from rest_framework.permissions     import IsAuthenticatedOrReadOnly

from .models import LevelModel, SpaceModel, SpaceType
from .services import LevelServices
from .permissions import LevelPermission
from .serializers import LevelSerializer


class LevelView(APIView):
    
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, LevelPermission]
    
    def post(self, request):
        
        serializer = LevelSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        new_level = LevelModel.objects.create(
            fill_priority = request.data['fill_priority'],
            name = request.data['name']
            )
        
        LevelServices.set_available_spaces(request.data, new_level)
            
        serializer = LevelSerializer(new_level).data
        
        return Response(serializer, status=status.HTTP_201_CREATED)
    
    def get(self, request):
        
        all_levels = get_list_or_404(LevelModel)
        levels_serializer = LevelSerializer(all_levels, many=True).data
        
        return Response(levels_serializer, status=status.HTTP_200_OK)
