# Parking Lot API

API para controle de entrada de veículos em um estacionamento com as seguintes Models:

- Admin (username=CharField, password=CharField, is_superuser=True, is_staff=True)
- LevelModel (name=CharField, fill_priority=InterField, motorcycle_spaces=IntegerField, car_spaces=IntegerField) relação 1:N com VehicleModel
- PricingModel (a_coefficient=IntegerField, b_coefficient=IntegerField)
- VehicleModel (vehicle_type=CharField - 'car' ou 'motorcycle', license_plate=ChardField, arrived_at=DateTimeField - auto_now_add , paid_at=DateTimeField - gerado automaticamente na saída do veículo, amount_paid=IntegerField - gerado automaticamente na saída do veículo)

## Regras para utilização das rotas

- Qualquer usuário, mesmo sem __token__, tem permissão para __GET__ em __LevelView__, __POST__ e __PUT__ em __VehicleModel__
- Somente um usuário __Admin__ (is_superuser=True, is_staff=True) tem permissão para __POST__ em __PrincingModel__ e __LevelModel__
- Para __POST__ e __PUT__ em __VehicleView__ é necessário que haja registro de __PrincingModel__ e __LevelModel__, também é necessário que haja __car_spaces__ ou __motorcycle_spaces__ > 0 em algum __LevelModel__

## Instalação

- python3 -m venv venv
- pip install django djangorestframework

## Utilização / Rotas

### POST "/api/accounts/"

```
-- REQUEST
{
  "username": "admin",
  "password": "1234",
  "is_superuser": true,
  "is_staff": true
}
```
```
-- RESPONSE STATUS -> HTTP 201
{
  "id": 1,
  "username": "admin",
  "is_superuser": true,
  "is_staff": true
}
```

### POST "/api/login/"

```
-- REQUEST
{
  "username": "admin",
  "password": "1234"
}
```
```
-- REQUEST STATUS -> HTTP 200
{
  "token": "0f500a8c2f2f8d5e77ad86b3a8d373a0528d8812"
}
```

### POST "/api/levels/" 
- criando um novo nível

```
-- REQUEST 
-- Header -> Authorization: Token <token-do-admin>
{
  "name": "floor 1",
  "fill_priority": 2,
  "motorcycle_spaces": 20,
  "car_spaces": 50
}
```
```
-- RESPONSE STATUS -> HTTP 201
{
  "id": 1,
  "name": "floor 1",
  "fill_priority": 2,
  "available_spaces": {
    "available_motorcycle_spaces": 20,
    "available_car_spaces": 50
  }
}
```

### GET "/api/levels/" 
- listando os níveis
```
-- RESPONSE STATUS -> HTTP 200
[
  {
    "id": 1,
    "name": "floor 1",
    "fill_priority": 5,
    "available_spaces": {
      "available_motorcycle_spaces": 20,
      "available_car_spaces": 50
    }
  },
  {
    "id": 2,
    "name": "floor 2",
    "fill_priority": 3,
    "available_spaces": {
      "available_motorcycle_spaces": 10,
      "available_car_spaces": 30
    }
  }
]
```

### POST "/api/pricings/" 
- criando uma nova precificação:
```
-- REQUEST 
-- Header -> Authorization: Token <token-do-admin>
{
  "a_coefficient": 100,
  "b_coefficient": 100
}
-- RESPONSE STATUS -> HTTP 201
{
  "id": 1,
  "a_coefficient": 100,
  "b_coefficient": 100
}
```

### POST "/api/vehicles/" 
- criando um novo registro de entrada:
```
-- REQUEST
{
  "vehicle_type": "car",
  "license_plate": "AYO1029"
}
```
```
-- RESPONSE STATUS -> HTTP 201
{
  "id": 1,
  "license_plate": "AYO1029",
  "vehicle_type": "car",
  "arrived_at": "2021-01-25T17:16:25.727541Z",
  "paid_at": null,
  "amount_paid": null,
  "space": {
    "id": 2,
    "variety": "car",
    "level_name": "floor 1"
  }
}
```
ou
```
-- RESPONSE STATUS -> HTTP 404
```

### PUT "/api/vehicles/<int:vehicle_id>/" 
- registrando a saída e pagamento do veículo:
```
-- REPONSE STATUS -> HTTP 200
{
  "license_plate": "AYO1029",
  "vehicle_type": "car",
  "arrived_at": "2021-01-21T19:36:55.364610Z",
  "paid_at": "2021-01-21T19:37:23.016452Z",
  "amount_paid": 100,
  "space": null
}
```
ou
```
-- RESPONSE STATUS -> HTTP 404
```