from rest_framework import serializers
from .models import Admin


class AdminSerializer(serializers.Serializer):
    
    id =            serializers.IntegerField(read_only=True)
    username =      serializers.CharField()
    is_superuser =  serializers.BooleanField()
    is_staff =      serializers.BooleanField()
    
class AuthSerializer(serializers.Serializer):
    
    username =      serializers.CharField()
    password =      serializers.CharField()