from rest_framework.response            import Response
from rest_framework.views               import APIView
from rest_framework                     import status

from rest_framework.authentication      import TokenAuthentication
from rest_framework.permissions         import IsAuthenticated

from .serializers import AdminSerializer, AuthSerializer
from .models import Admin
from .services import AdminServices


class SignUpView(APIView):
    
    def post(self, request):
        
        this_admin_data = request.data
        serializer = AdminSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        new_admin = AdminServices.create_admin(this_admin_data)
        serializer = AdminSerializer(new_admin).data
        
        return Response(serializer, status=status.HTTP_201_CREATED)
        
class LoginView(APIView):
    
    def post(self, request):
        
        serializer = AuthSerializer(data=request.data)
        
        if not serializer.is_valid():
            
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        
        token = AdminServices.get_token_or_401(request.data)
        
        if token == 401:
            
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        
        return Response({'token': token.key}, status=status.HTTP_200_OK)