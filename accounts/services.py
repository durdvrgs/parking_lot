from django.contrib.auth                import authenticate
from rest_framework.authtoken.models    import Token

from .models import Admin


class AdminServices():
    
    @staticmethod
    def create_admin(admin_data):
        
        new_admin = Admin.objects.create_user(
            username=admin_data['username'],
            password=admin_data['password'],
            is_superuser=admin_data['is_superuser'],
            is_staff=admin_data['is_staff']
        )
        
        return new_admin
    
    @staticmethod
    def get_token_or_401(admin_credentials):
        
        admin = authenticate(
            username=admin_credentials['username'],
            password=admin_credentials['password']
        )
        
        if not admin:
            return 401
        
        return Token.objects.get_or_create(user=admin)[0]
        